# kenton.zsh-theme #

This is my custom [OH MY ZSH](https://github.com/robbyrussell/oh-my-zsh) theme.

![theme screenshot](https://github.com/notnek/zsh-theme/raw/master/screenshot.png)